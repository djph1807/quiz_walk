//
//  StartButton.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-12-08.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit

class StartButton: UIButton {
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpButton()
    }
    
    private func setUpButton() {
        layer.cornerRadius = 25
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 8
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        clipsToBounds = true
        layer.masksToBounds = false
    }
    
}
