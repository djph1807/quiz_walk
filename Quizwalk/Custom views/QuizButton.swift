//
//  QuizButton.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-13.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit


class QuizButton: UIButton {
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpButton()
    }
    
    private func setUpButton() {
        backgroundColor     = UIColor.white
        titleLabel?.font    = UIFont(name: "avenirNextCondenseDemiBold", size: 14)
        layer.cornerRadius  = frame.size.height/4
        layer.borderWidth   = 2
        layer.borderColor   = UIColor.systemBlue.cgColor
        setTitleColor(.systemBlue, for: .normal)
    }
    
}


