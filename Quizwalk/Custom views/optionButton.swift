//
//  optionButton.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-12-02.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit


class OptionButton: UIButton {
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpButton()
    }
    
    private func setUpButton() {
        backgroundColor     = UIColor.gray
        titleLabel?.font    = UIFont(name: "avenirNextCondenseDemiBold", size: 22)
        layer.cornerRadius  = 25
        setTitleColor(.white, for: .normal)
    }
}

extension OptionButton {
    
    func flashy(){
        let flashbutton = CABasicAnimation(keyPath: "opacity")
        flashbutton.duration = 0.5
        flashbutton.fromValue = 1
        flashbutton.toValue = 0.1
        flashbutton.autoreverses = true
        flashbutton.repeatCount = 1
        
        layer.add(flashbutton, forKey: nil)
    }
}
