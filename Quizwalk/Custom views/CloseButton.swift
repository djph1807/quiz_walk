//
//  CloseButton.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-12-06.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit

class CloseButton: UIButton {
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpButton()
    }
    
    private func setUpButton() {
        backgroundColor     = UIColor.systemBlue
        titleLabel?.font    = UIFont(name: "avenirNextCondenseDemiBold", size: 22)
        layer.cornerRadius  = frame.size.height/2
        setTitleColor(.white, for: .normal)
    }
    
}
