//
//  StartViewController.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-07.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit


class StartViewController: UIViewController {
    
    // MARK: - Custom UI elements
    @IBOutlet var myView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var AnimationImage: UIImageView!
    
    // MARK: - View UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let runningAnimation: [UIImage] = Animation.instance.createImageArray(total: 12, imagePrefix: "caveman")
        Animation.instance.animate(imageView: AnimationImage, images: runningAnimation)
        let animation = Animation.instance.changeColor()
        view.layer.add(animation, forKey: "backgroundColor")
    }
    
}


