//
//  QuizViewController.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-12.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit
import AudioToolbox


class QuizViewController: UIViewController {

    // MARK: - Custom UI elements
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var questionCounter: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var choice1: UIButton!
    @IBOutlet weak var choice2: UIButton!
    @IBOutlet weak var choice3: UIButton!
    @IBOutlet weak var choice4: UIButton!
    @IBOutlet weak var backButton: UIButton!
  
    var correctAnswer: Int?
    var questionNumber: Int?
    var listOfQuestion = [Question]()
    
    
    // MARK: - VIEW UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButton.isEnabled = false
    
        getQuestions()
        updateUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        let runningAnimation: [UIImage] = Animation.instance.createImageArray(total: 4, imagePrefix: "question")
        Animation.instance.animate(imageView: questionImage , images: runningAnimation)
    }
    
    // Update the custom UI
    // Parameters: NONE
    // Return: NONE
    func updateUI(){
        scoreLabel.text = "Score: \(Global.instance.score)"
        questionCounter.text = "Question: \(questionNumber!)"
    }
    
    // MARK: - View Button Aciton
    // Check if user have answered the question
    // Parameters: UIButton
    // Return: NONE
    @IBAction func answerButtonClicked(_ sender: UIButton) {
        
        backButton.isEnabled = true
       
        if Global.instance.didAnswered[questionNumber! - 1] == false {
            if sender.tag == correctAnswer {
                sender.backgroundColor = UIColor.green
                Global.instance.score += 1
                updateUI()
            }
            else{
                sender.backgroundColor = UIColor.red
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            Global.instance.didAnswered[questionNumber! - 1] = true
        }
    }
   
    // MARK: - JSONFILE API
    // Get's the questions from the json file and then updates the UI
    // Parameters: NONE
    // Return: NONE
    func getQuestions() {
        
        // Checking URL
        guard let url = URL(string: "https://api.myjson.com/bins/1a432e") else {
            print("Något gick fel med URL")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                print("Ingen data")
                return
            }
            // decoding json
            do {
                let decoder = JSONDecoder()
                print(decoder)
                let decodedQuestions = try decoder.decode(QuestionResult.self, from: data)
                let decodedQuestionsDetail = decodedQuestions.dict
                self.listOfQuestion.append(contentsOf: decodedQuestionsDetail)
                
                // Updates the UI elements
                DispatchQueue.main.async {
                    if Global.instance.easyQuestions == true {
                        self.questionLabel.text = self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].question
                        self.choice1.setTitle(self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].answers[0], for: UIControl.State.normal)
                        self.choice2.setTitle(self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].answers[1], for: UIControl.State.normal)
                        self.choice3.setTitle(self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].answers[2], for: UIControl.State.normal)
                        self.choice4.setTitle(self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].answers[3], for: UIControl.State.normal)
                        self.correctAnswer = self.listOfQuestion[0].dictEasy[self.questionNumber! - 1].correct_answer
                    }
                    else if Global.instance.hardQuestions == true {
                        self.questionLabel.text = self.listOfQuestion[0].dictHard[self.questionNumber! - 1].question
                        self.choice1.setTitle(self.listOfQuestion[0].dictHard[self.questionNumber! - 1].answers[0], for: UIControl.State.normal)
                        self.choice2.setTitle(self.listOfQuestion[0].dictHard[self.questionNumber! - 1].answers[1], for: UIControl.State.normal)
                        self.choice3.setTitle(self.listOfQuestion[0].dictHard[self.questionNumber! - 1].answers[2], for: UIControl.State.normal)
                        self.choice4.setTitle(self.listOfQuestion[0].dictHard[self.questionNumber! - 1].answers[3], for: UIControl.State.normal)
                        self.correctAnswer = self.listOfQuestion[0].dictHard[self.questionNumber! - 1].correct_answer
                    }
                }
            }
            catch {
                print("fel med datahanteringen")
                return
            }
        }.resume()
    }
}

