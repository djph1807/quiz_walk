//
//  DifficultandRulesViewController.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-11-15.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit


class DifficultandRulesViewController: UIViewController {
    
    // MARK: - Custom UI elements
    let swiftColor = UIColor(red: 0, green: 255, blue: 0, alpha: 0.4)
    @IBOutlet weak var HardButton: UIButton!
    @IBOutlet weak var EasyButton: UIButton!
    @IBOutlet weak var PlayGameButton: UIButton!
   
    // MARK: - View UI
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateButton()
    }
    
    // PlayGameButton is disable
    // Parameters: NONE
    // Return: NONE
    func updateButton(){
        PlayGameButton.isEnabled = false

        if PlayGameButton.isEnabled == false {
            PlayGameButton.backgroundColor = swiftColor
        }
    }
    
    // MARK: - View Button Action
    // Depending on what button is pressed it fetches the easy or the hard questions
    // Parameters: UIButton sender
    // Returns: NONE
    @IBAction func optionClicked(_ sender: OptionButton) {
        sender.flashy()
        if sender.tag == 1 {
            EasyButton.backgroundColor = UIColor.systemGreen
            HardButton.backgroundColor = UIColor.gray
            Global.instance.easyQuestions = true
            Global.instance.hardQuestions = false
        }
        else {
            HardButton.backgroundColor = UIColor.systemRed
            EasyButton.backgroundColor = UIColor.gray
            Global.instance.hardQuestions = true
            Global.instance.easyQuestions = false
        }
        PlayGameButton.isEnabled = true
        PlayGameButton.backgroundColor = UIColor.systemGreen
    }
    
    // UnwindSegue for restart button at end screen
    // Parameters: unwindSegue
    // Returns: NONE
    @IBAction func restartGame(_unwindSegue: UIStoryboardSegue){
        Global.instance.restartQuiz()
    }
}

