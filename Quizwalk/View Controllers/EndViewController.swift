//
//  EndViewController.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-11-15.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit

class EndViewController: UIViewController {
    
    // MARK: - Custom UI elements
    var highScore = UserDefaults().integer(forKey: "Record")
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    // MARK: - View UI
    override func viewWillAppear(_ animated: Bool) {
        cornerRadius()
        endGame()
    
        let userDefaults = UserDefaults.standard
        let value = userDefaults.string(forKey: "Record")
        label2.text = "\(Global.instance.score)"
        label4.text =  value
        
        if (value == nil) {
            label2.text = "0"
            label4.text = "0"
        }else{
            label4.text = value
        }
    }
    
    // Customize the buttons
    // Parameters: NONE
    // Return : NONE
    func cornerRadius(){
        shareButton.layer.cornerRadius = 20
        restartButton.layer.cornerRadius = 20
    }
    
    // endGame() sets highscore to score if the score is greater than highscore
    // Parameter: NONE
    // Return: NONE
    func endGame() {
        label2.text = "\(Global.instance.score)"
        label4.text = "\(highScore)"
     
        if (Global.instance.score > highScore) {
            highScore = Global.instance.score
            let userDefaults = UserDefaults.standard
            userDefaults.set(highScore, forKey: "Record")
           }
    }
    
    // MARK: - View Button Action
    // What will appear when the share button is pressed, UIAVC handels custom service(SMS,copy etc)
    // Parameters:
    // Return: NONE
    @IBAction func shareButtonPressed(_ sender: Any) {
        
        let text:NSString = NSString(format: "I just scored: %i on Quizwalk!", Global.instance.score)
        let activity = UIActivityViewController(activityItems: [text], applicationActivities: [])
        if let popoverController = activity.popoverPresentationController{
            popoverController.sourceView = self.view
            popoverController.sourceRect = self.view.bounds
        }
        self.present(activity, animated: true, completion: nil)
    }
}

