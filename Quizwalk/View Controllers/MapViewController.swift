//
//  MapViewController.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-16.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class MapViewController: UIViewController {
    
    
    // MARK: - Custom UI elements
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scoreLabel: UILabel!
    
    let regionRadius: CLLocationDistance = 2500
    let initialLocation = CLLocation(latitude: 57.78145, longitude: 14.15618)
    let locationManager = CLLocationManager()
    var currentCoordinate: CLLocationCoordinate2D!
    var route = MKRoute()
    
    // MARK: - VIEW UI
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        mapView.addAnnotations(DataHandler.instance.locations)
    }
    
    // updateView() updates the view on the map and where user is
    // Parameters: NONE
    // Return: NONE
    func updateView() {
        scoreLabel.text = "Score: \(Global.instance.score)"
        mapView.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        centerMapOnLocation(location: initialLocation)
        
    }
    
    // MARK: - View Button Action
    // Creates an alert to the view when clicking on finishGameButton
    // Parameters: 
    // Return: NONE
    @IBAction func finishGameClicked(_ sender: Any) {
        let alertFinish = UIAlertController(title: "Hello!", message: "Do you wish to continue playing or do you want to finish the game?", preferredStyle: .alert)
        let playGame = UIAlertAction(title: "Keep playing", style: .default, handler: nil)
        alertFinish.addAction(playGame)
        let highScore = UIAlertAction(title: "Finish Game", style: .default, handler:{ (action) -> Void in self.performSegue(withIdentifier: "highscore", sender: self)})
        alertFinish.addAction(highScore)
        present(alertFinish, animated: true, completion: nil)
    }
    
    // MARK: - Unwind Segue sender
    // Update score when closing popover in QuizViewController
    // Parameters: unwindSegue from QuizViewController
    // Return: NONE
    @IBAction func updateScore(_unwindSegue: UIStoryboardSegue) {
        scoreLabel.text = "Score: \(Global.instance.score)"
        /* {
            let alert = UIAlertController(title: "End of quiz!", message: "Push Finish Game to end the quiz.", preferredStyle: .alert)
            let finishGame = UIAlertAction(title: "Finish Game", style: .default, handler: { (action) -> Void in self.performSegue(withIdentifier: "highscore", sender: self)})
            alert.addAction(finishGame)
            present(alert, animated: true, completion: nil)
        }*/
    }
    
    // MARK: - Segue sender
    // Notifies the view controller that a segue is about to be preformed
    // Parameters: The segue object containing information about the view controllers involved in the segue.
    // Return: NONE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "question"){
            if let destination = segue.destination as? QuizViewController {
                let segueSender: Location? = sender as? Location
                destination.questionNumber = segueSender!.questionNumber
            }
        }
    }
}

// MARK: - Location Delegate
extension MapViewController: CLLocationManagerDelegate {
    
    // centerMapOnLocation center the map on Jönköping with specific coordinates
    // Parameters: location
    // Return: NONE
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // Create the user icon on the map
    // Parameters: manager object that generate update event & locations array objects containing the location data
    // Return: NONE
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        guard let currentLocation = locations.first else { return }
        currentCoordinate = currentLocation.coordinate
        mapView.userTrackingMode = .followWithHeading
    }
}
    

// MARK: - MapView Delegate
extension MapViewController: MKMapViewDelegate {
    
    // Returns the view associated with the specifed annotation object
    // Parameters: mapView that request the annotation view. annotation object that represent thats is about to be displayed.
    // Return: The annotation view to display for specified annotation or nil if you want to display a standard annotation view.
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? Location else { return nil}
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        let questionButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 30, height: 30)))
        questionButton.setBackgroundImage(UIImage(named: "questionmark"), for: UIControl.State())
  
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.markerTintColor = UIColor.blue
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.glyphTintColor = UIColor.white
            view.leftCalloutAccessoryView = questionButton
            let infoButton = UIButton(type: .detailDisclosure)
            view.rightCalloutAccessoryView = infoButton as UIView
        }
        return view
    }
    
    // Tells the delegate that one of its annotation views was deselected
    // Parameters: mapView containing the annotation view and view object that is the annotation that was deselected
    // Return: NONE
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        mapView.removeOverlay(route.polyline)
    }
    
    // Tells the delegate that the user tapped one of the annotation view's accessory buttons.
    // Parameters: mapView containing the specified annotation view. The annotation view whose button was tapped. The control that was tapped.
    // Return: NONE
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        print(route.distance)
        if self.route.distance < 100 {
            if control == view.leftCalloutAccessoryView {
                performSegue(withIdentifier: "question", sender: view.annotation)
                mapView.removeAnnotation(view.annotation!)
                print(route.distance)
                
            }
        }
        if control == view.rightCalloutAccessoryView {
            let alertInfo = UIAlertController(title: "Question", message:NSString(format: "Click on the questionmarker when you are nearby the question place! (0-100m) You are %.1f from the question", route.distance) as String ,preferredStyle: .alert)
            let closeAction = UIAlertAction(title: "Close", style: .default, handler: {action in alertInfo.dismiss(animated: true, completion: nil)})
            alertInfo.addAction(closeAction)
            present(alertInfo, animated: true, completion: nil)
        }
    }
    
    // Tells the delegate that one of its annotation views was selected
    // Parameters: mapView containing the annotation view. The annotation view that was selected
    // Return: NONE
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else {return}
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem.forCurrentLocation()
        directionRequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: annotation.coordinate))
        directionRequest.transportType = .walking
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                return
            }
        
            if !response.routes.isEmpty {
                self.route = response.routes[0]
                DispatchQueue.main.async { [weak self] in
                    self?.mapView.addOverlay(self!.route.polyline)
                }
            }
        }
        
    }
    
    // Asks the delegate for a renderer object to use when drawing the specified overlay
    // Parameters: mapView that requested the renderer object. The overlay object that is about to be displayed.
    // Return: The renderer to use when presenting the specified overlay on the map
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard overlay is MKPolyline else { return MKPolylineRenderer() }
        
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = UIColor.blue
        polylineRenderer.fillColor = UIColor.red
        polylineRenderer.lineWidth = 2
        return polylineRenderer
    }
}





         
