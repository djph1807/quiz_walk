//
//  Question.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-15.
//  Copyright © 2019 Projekt. All rights reserved.
//

import Foundation

class QuestionResult: Codable {
    let dict: [Question]
    
    init(dict: [Question]){
        self.dict = dict
    }
}

class Question: Codable {
    let dictEasy: [QuestionDetail]
    let dictHard: [QuestionDetail]
    
    init( dictEasy: [QuestionDetail], dictHard: [QuestionDetail]){
        self.dictEasy = dictEasy
        self.dictHard = dictHard
    }
}

class QuestionDetail: Codable {
    let question: String
    let correct_answer: Int
    let answers: [String]
    
    init(question: String, correct_answer: Int, answers: [String]) {
        self.question = question
        self.correct_answer = correct_answer
        self.answers = answers
    }
    
}

