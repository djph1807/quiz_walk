//
//  Global.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-21.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit

class Global {

    static let instance = Global()
    
    var score: Int
    var answer: Bool
    var hardQuestions: Bool
    var easyQuestions: Bool
    var didAnswered = [Bool]()
    

    init() {
        self.score = 0
        self.answer = false
        self.hardQuestions = false
        self.easyQuestions = false
        
        for _ in 0...9 {
            didAnswered.append(answer)
        }
    }
    
    // Restart the quizGame
    // Parameters: NONE
    // Return: NONE
    func restartQuiz() {
        score = 0
        didAnswered.removeAll()
        for _ in 0...9 {
            didAnswered.append(answer)
        }
    }
}




