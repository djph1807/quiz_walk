//
//  Animation.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-12-02.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit

class Animation {
    
    static let instance = Animation()
    
    func createImageArray(total:Int, imagePrefix:String) -> [UIImage]{
        
        var imageArray: [UIImage] = []
        
        for imageCount in 1..<total {
            let imageName = "\(imagePrefix)-\(imageCount).png"
            let image = UIImage(named: imageName)!
            
            imageArray.append(image)
        }
        return imageArray
    }
    
    func animate(imageView: UIImageView, images: [UIImage]){
        imageView.animationImages = images
        imageView.animationDuration = 1
        imageView.animationRepeatCount = 100
        imageView.startAnimating()
    }
    
    func changeColor() -> CABasicAnimation{
       
        let animation = CABasicAnimation(keyPath: "backgroundColor")
        animation.fromValue = UIColor.cyan.cgColor
        animation.toValue = UIColor.blue.cgColor
        animation.duration = 2
        animation.beginTime = CACurrentMediaTime() + 0.3
        animation.autoreverses = true
        animation.repeatCount = 100
        
        return animation
    }
}
