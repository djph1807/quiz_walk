//
//  DataHandler.swift
//  Quizwalk
//
//  Created by Emil Christoffersson on 2019-12-02.
//  Copyright © 2019 Projekt. All rights reserved.
//

import UIKit
import MapKit



class DataHandler {
    
    static let instance = DataHandler()
    
    
    let locations: [Location] = [
        Location(title: "Jönköping Tekniska Högskola", locationName: "Jönköping University", discipline: "School", coordinate: CLLocationCoordinate2D(latitude: 57.7786, longitude: 14.1637), questionNumber:1),
        Location(title: "Science Park", locationName: "Västra Centrum", discipline: "Company", coordinate: CLLocationCoordinate2D(latitude: 57.7793764, longitude: 14.159859), questionNumber: 2),
        Location(title: "Idrottshuset", locationName: "Västra Centrum", discipline: "Sports hall", coordinate: CLLocationCoordinate2D(latitude: 57.78169427995794, longitude: 14.153899185180666), questionNumber:3),
        Location(title: "Slottskyrkogården", locationName: "Västra Centrum", discipline: "Graveyard", coordinate: CLLocationCoordinate2D(latitude: 57.7834151, longitude: 14.1553207), questionNumber: 4),
        Location(title: "Tändsticksmusset", locationName: "Tändsticksområdet", discipline: "Museum", coordinate: CLLocationCoordinate2D(latitude: 57.78466770787568, longitude: 14.159660514186099), questionNumber: 5),
        Location(title: "Piren", locationName: "Östra Centrum", discipline: "Pier", coordinate: CLLocationCoordinate2D(latitude: 57.78536604457727, longitude: 14.168760625022898), questionNumber: 6),
        Location(title: "Kulturhuset Spira", locationName: "Östra Centrum", discipline: "Music and Theater center", coordinate: CLLocationCoordinate2D(latitude: 57.777712594895, longitude: 14.172011462348948), questionNumber:7),
        Location(title: "Knektaparken", locationName: "Östra Centrum", discipline: "Park", coordinate: CLLocationCoordinate2D(latitude: 57.777593, longitude: 14.188654), questionNumber:8),
        Location(title: "A6 Center", locationName: "A6 Ryhov", discipline: "Shopping Center", coordinate: CLLocationCoordinate2D(latitude: 57.773062, longitude: 14.203546), questionNumber:9),
        Location(title: "Vätterstranden", locationName: "Liljeholmen", discipline: "Beach", coordinate: CLLocationCoordinate2D(latitude: 57.782123, longitude: 14.192005), questionNumber: 10)
    ]
    
    
}
