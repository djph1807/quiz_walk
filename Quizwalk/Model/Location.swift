//
//  Artwork.swift
//  Quizwalk
//
//  Created by Philip Djup on 2019-11-16.
//  Copyright © 2019 Projekt. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class Location: NSObject, MKAnnotation {

    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    let questionNumber: Int

    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, questionNumber: Int) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.questionNumber = questionNumber
    
        super.init()
    }
    
    var subtitle: String?{
        return locationName
    }
    
}
