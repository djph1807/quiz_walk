We have decided to create an quiz app but not just an normal quiz app where you can sit at home and do it.
In order to do this quiz the user has to move and travel through Jönköping and answer different questions about the city.
The first page will contain a ’Log in’ and a ’Create user’ button that will redirect to a new view controller.
Also on the first page there are a question mark box that explains some information about the application.
In create user the user can choose a username, password and also an email(optional).
If the username already exist the user cant be created and need to put in a new username that doesn’t exist in swifts database.
When either the user is logged in or has created a new user they will be redirected to the rules page which will explain which rules there are and also how the game works.
Here the user can choose between easy questions or hard questions for the game to play.
The user must chose easy or hard in order to play the game, when either of the difficulties are checked in the user can press play and proceed to play the game.
It is possible to check the scoreboard from this page as well.
When the game starts a map is fetched and the user can se where they are and also where the questions are around the town.
The questions will appear on the map like small boxes with question marks on them.
If the user is around 0-50m from the question the user can press the box on the map and a question will appear in a pop-up window.
There will be a question and four alternative answers the user can choose from.
If the user doesn’t know how to walk around Jönköping there will be a ‘route’ button that will guide them from one question to another the shortest way.
If the user has had to much exercise and doesn’t want to play more they can press ‘submit game’ that will redirect them to the scoreboard that will show a table view with to different section easy and a hard section depending on what the user choose before the game started.
In the table view if will show how many questions they chose to answer and how many of them were correct and also the name of the user.
If the user press submit game and haven’t finished all the questions a pop-up will appear and ask the user if they are really sure to finish the game when all the questions hasn’t been answered.
If the user has finished all the questions they will automatically be redirected to the scoreboard.
In the scoreboard users can press ‘play again’ and be redirected to the ‘rules page’ again where they can choose between easy/hard again.
We will have an navigation controller so the users can go between the different controllers and not be stuck.